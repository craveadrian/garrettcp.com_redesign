	<div id="content">
		<div class="row">
			<h1>Gallery</h1>
			<div id="gall1" class="gallery-container">
				<h4>Cabinets</h4>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "cabinets") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall2" class="gallery-container">
				<h4>Furnitures</h4>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "furnitures") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall3" class="gallery-container">
				<h4>Faux</h4>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "faux") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall4" class="gallery-container">
				<h4>Flooring</h4>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "flooring") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall5" class="gallery-container">
				<h4>Exterior Painting</h4>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "exterior-painting") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
			<div id="gall6" class="gallery-container">
				<h4>Interior Painting</h4>
				<ul class="gallery clearfix" >
					<?php foreach ($gallery as $gall) {  if($gall["rel"] == "interior-painting") { ?>
					<li>
						<a data-fancybox-group="<?php echo $gall["rel"]?>" class="thumbnail fancy" title="<?php echo $gall["src"]?>" href="public/images/gallery/<?php echo $gall["src"]?>.jpg">
							<img class="img-responsive" src="public/images/gallery/tm/<?php echo $gall["src"];?>.jpg" alt="<?php echo $gall["alt"];?>">
						</a>
					</li>
					<?php }} ?>
				</ul>
				<div class="page_navigation"></div>
			</div>
		</div>
	</div>
