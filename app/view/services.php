<div id="content">
	<div class="row">
		<h1>Services</h1>
		<h4>Maintenance and Repair.</h4>
		<p>When our homes or places of business find themselves in need of repair or have been damaged it's important to have as little disruption to our everyday lives as possible. Our goal at Garrett Custom Painting is to take care of the problem and help restore some normalcy to your life as quickly as possible. We can take any damaged area and restore it to the beautiful condition it once was.</p>

		<p>We can restore everything from your cabinets and furniture to your plaster or painted walls, even if those walls have a unique finish especially designed just for you. We can even repair the wood, concrete, stone, or stucco on the exterior of your home.</p>

		<h4>Drywall Repair Services</h4>
		<p>Content Comming Soon...</p>

		<h4>Faux Painting Finish Services</h4>
		<p>Content Comming Soon...</p>

		<h4>Painting Services</h4>
		<p>Content Comming Soon...</p>
	</div>
</div>
