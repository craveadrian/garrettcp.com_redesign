<div id="what-we-do" class="resSection">
	<div class="row">
		<h2>WHAT WE DO</h2>
		<h5>Complete home Repairs and Painting</h5>
		<div class="services">
			<dl class="inb">
				<dt> <img src="public/images/content/service1.jpg" alt="Service Image"> </dt>
				<dd> WATER & FLOOD <br> DAMAGE REPAIR</dd>
			</dl>
			<dl class="inb resMiddle">
				<dt> <img src="public/images/content/service2.jpg" alt="Service Image"> </dt>
				<dd> PAINTING SERVICES </dd>
			</dl>
			<dl class="inb">
				<dt> <img src="public/images/content/service3.jpg" alt="Service Image"> </dt>
				<dd> CABINET & FURNITURE <br> FINISHING</dd>
			</dl>
		</div>
	</div>
</div>
<div id="welcome" class="resSection">
	<div class="row">
		<div class="col-8 fr">
			<div class="wlcTop">
				<p>WELCOME TO</p>
				<h1> Garrett<span>Custom Painting</span> </h1>
				<p>Garrett Custom Painting is a family owned Company with a reputation for excellence. We have enjoyed working with some of the top builders, designers, property management companies and homeowners here in the Aspen Valley since 1987.</p>
				<p>Our Company offers a variety of services for your home or business. Please enjoy our Galleries for some ideas on your next project. </p>
				<a href="service#content" class="btn">LEARN MORE</a>
			</div>
			<div class="wlcBot">
				<img src="public/images/content/welcomeImg1.jpg" alt="Welcome Image 1">
				<img src="public/images/content/welcomeImg2.jpg" alt="Welcome Image 2" class="middle">
				<img src="public/images/content/welcomeImg3.jpg" alt="Welcome Image 3">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="recent-works" class="resSection">
	<div class="row">
		<h2>RECENT WORKS</h2>
		<h5>Our pledge to you… On time done right</h5>
		<div class="rwImages">
			<img src="public/images/content/work1.jpg" alt="Work 1">
			<img src="public/images/content/work2.jpg" alt="Work 2" class="middle">
			<img src="public/images/content/work3.jpg" alt="Work 3">
			<img src="public/images/content/work4.jpg" alt="Work 4">
			<img src="public/images/content/work5.jpg" alt="Work 5" class="middle">
			<img src="public/images/content/work6.jpg" alt="Work 6">
		</div>
	</div>
</div>
<div id="services2">
	<div class="svc2Top">
		<div class="row">
			<h2>Uncompromising Quality and Service.</h2>
			<h5>Getting the job done wherever, however, no matter how big or small.</h5>
		</div>
	</div>
	<div class="svc2Bot">
		<div class="row">
			<dl>
				<dt> <img src="public/images/content/svc1.jpg" alt="Service2 1"> </dt>
				<dd>
					<p>Thank You!</p>
					<p>I would recommend Garrett Custom Painting to anyone looking for cleaning services for their office needs.</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svc2.jpg" alt="Service2 2"> </dt>
				<dd>
					<p>Satisfaction Guaranteed</p>
					<p>I have been utilizing Garrett Custom Painting going on five years now.</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svc3.jpg" alt="Service2 3"> </dt>
				<dd>
					<p>Recommended</p>
					<p>The crew is not only timely but also efficient in continually taking care of our needs.</p>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="map" class="resSection">
	<div class="row">
		<a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info('company_name'); ?> Main Logo"> </a>
	</div>
</div>
<div id="contact">
		<div class="cntLeft col-6 fl resSection">
			<div class="row">
				<div class="container">
					<h3>WHY CHOOSE US</h3>
					<ul>
						<li> <img src="public/images/common/sprite.png" alt="Bullet 1" class="bg-wcu1"> <p>WE GOT THE TOOLS</p> </li>
						<li> <img src="public/images/common/sprite.png" alt="Bullet 2" class="bg-wcu2"> <p>CERTIFIED EXPERIENCE</p> </li>
						<li> <img src="public/images/common/sprite.png" alt="Bullet 3" class="bg-wcu3"> <p>COMPETITIVE PRICE</p> </li>
						<li> <img src="public/images/common/sprite.png" alt="Bullet 4" class="bg-wcu4"> <p>30 YEARS EXPERIENCE</p> </li>
						<li> <img src="public/images/common/sprite.png" alt="Bullet 5" class="bg-wcu5"> <p>GREAT SUPPORT</p> </li>
					</ul>
				</div>
			</div>
		</div>
		<div class="cntRight col-6 fr resSection">
			<div class="container">
				<div class="row">
					<h3>CONTACT US</h3>
					<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
						<label><span class="ctc-hide">Name</span>
							<input type="text" name="name" placeholder="Name:">
						</label>
						<label><span class="ctc-hide">Address</span>
							<input type="text" name="address" placeholder="Address:">
						</label>
						<label><span class="ctc-hide">Email</span>
							<input type="text" name="email" placeholder="Email:">
						</label>
						<label><span class="ctc-hide">Phone</span>
							<input type="text" name="phone" placeholder="Phone:">
						</label>
						<label><span class="ctc-hide">Message</span>
							<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
						</label>
						<div class="g-recaptcha"></div>
						<input type="checkbox" name="consent" class="consentBox">
						<p class="check">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</p><br><br>
						<?php if( $this->siteInfo['policy_link'] ): ?>
						<input type="checkbox" name="termsConditions" class="termsBox"/>
						<p class="check">I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></p>
						<?php endif ?>
						<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
					</form>
				</div>
			</div>
		</div>
	<div class="clearfix"></div>
</div>
<div id="details">
	<div class="row">
		<div class="detLeft col-3 fl">
			<a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info('company_name'); ?> Main Logo"> </a>
		</div>
		<div class="detRight col-9 fr">
			<div class="container">
				<dl>
					<dt> <img src="public/images/common/sprite.png" alt="Phone Icon" class="bg-phone"> <p>PHONE:</p> </dt>
					<dd> <?php $this->info(["phone","tel"]); ?></dd>
				</dl>
				<dl>
					<dt> <img src="public/images/common/sprite.png" alt="Location Icon" class="bg-location"> <p>ADDRESS:</p> </dt>
					<dd> <?php $this->info("address"); ?></dd>
				</dl>
				<dl>
					<dt> <img src="public/images/common/sprite.png" alt="Email Icon" class="bg-email"> <p>EMAIL:</p> </dt>
					<dd> <?php $this->info(["email","mailto"]); ?></dd>
				</dl>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
